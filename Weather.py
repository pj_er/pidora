import urllib2
import json
import urllib
def get_forecast():
    """ 
    Makes a call to wunderground's API and displays three-day weather forecast. 
    """ 
    # Open url, make call to the API using an API key.
    f = urllib2.urlopen('http://api.wunderground.com/api/7d9eb4cb25e68fc9/forecast/q/55446.json')
    json_string = f.read() # Save response as a json string.
    parsed_json = json.loads(json_string) # Parse the json string.

    # If there is a forecast...
    if parsed_json:

        # Extract a 3-day forecast from the parsed json (elements 2 through the end).
        three_day_forecast = parsed_json['forecast']['txt_forecast']['forecastday'][2:]
        for day in three_day_forecast:
            forecast = day['title']+": "+day['fcttext']
            print forecast
    # Otherwise...
    else:
      print "No Forecast Today"
      
def get_Current()
      f = urllib2.urlopen('http://api.wunderground.com/api/7d9eb4cb25e68fc9/geolookup/conditions/q/MN/Plymouth.json') 
      json_string = f.read()
      parsed_json = json.loads(json_string) 
      location = parsed_json['location']['city'] 
      temp_f = parsed_json['current_observation']['temp_f'] 
      current = "Current temperature in %s is: %s" % (location, temp_f) 
      print current
      f.close()
 def main()
 	get_forcast()
 	get_Current()
